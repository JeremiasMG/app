from __future__ import unicode_literals
from django.db import models


class Usuarios(models.Model):
    Nombre = models.CharField(max_length=30)
    Usuario = models.CharField(max_length=30)
    Email = models.EmailField()
    Contraseña = models.CharField(max_length=30)


class Concato(models.Model):
    Nombre = models.CharField(max_length=30)
    Email = models.EmailField()
    Mensaje = models.TextField()


class Productos (models.Model):
    Precio = models.IntegerField()
    Stock = models.IntegerField()
    NombreProducto = models.CharField(max_length=30)
