# Generated by Django 3.2.4 on 2023-05-08 16:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0004_remove_concato_mensaje'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contacto',
            fields=[
                ('id', models.BigAutoField(auto_created=True,
                 primary_key=True, serialize=False, verbose_name='ID')),
                ('Nombre', models.CharField(max_length=30)),
                ('Email', models.EmailField(max_length=254)),
                ('Mensaje', models.TextField()),
            ],
        ),
        migrations.DeleteModel(
            name='Concato',
        ),
    ]
