<?php
// Configuración de la conexión a la base de datos
$host = "localhost";
$user = "username";
$password = "password";
$database = "database_name";

// Conexión a la base de datos
$conn = mysqli_connect($host, $user, $password, $database);
if (!$conn) {
    die("No se pudo conectar a la base de datos: " . mysqli_connect_error());
}

// Manejador de envío del formulario de inicio de sesión
if (isset($_POST["login"])) {
    $email = $_POST["email"];
    $password = $_POST["password"];

    // Consulta SQL para verificar las credenciales del usuario
    $sql = "SELECT * FROM users WHERE email='$email' AND password='$password'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) == 1) {
        // Autenticación exitosa
        session_start();
        $_SESSION["email"] = $email;
        header("Location: index.php");
    } else {
        // Autenticación fallida
        $error = "El correo electrónico o la contraseña son incorrectos.";
    }
}

// Manejador de envío del formulario de registro
if (isset($_POST["register"])) {
    $name = $_POST["name"];
    $email = $_POST["email2"];
    $password = $_POST["password2"];

    // Consulta SQL para verificar si el correo electrónico ya está registrado
    $sql = "SELECT * FROM users WHERE email='$email'";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) == 0) {
        // Correo electrónico no registrado, agregar nuevo usuario
        $sql = "INSERT INTO users (name, email, password) VALUES ('$name', '$email', '$password')";
        mysqli_query($conn, $sql);

        // Registro exitoso
        session_start();
        $_SESSION["email"] = $email;
        header("Location: index.php");
    } else {
        // Correo electrónico ya registrado
        $error = "El correo electrónico ya está registrado.";
    }
}

mysqli_close($conn);
?>